@extends('layouts.admin')
@section('content')
<div class="content">
    @can('user_create')
        <div style="margin-bottom: 10px;" class="row">
            <div class="col-lg-12">
                <a class="btn btn-success" onclick="window.location='/addEvent'">
                    Add Event
                </a>
            </div>
        </div>
    @endcan
    <div class="row">
        <div class="col-lg-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    {{ trans('cruds.user.title_singular') }} {{ trans('global.list') }}
                </div>
                <div class="panel-body">

                    <div class="table-responsive">
                        <table class=" table table-bordered table-striped table-hover datatable datatable-User">
                            <thead>
                                <tr>
                                    <th width="10">

                                    </th>
                                    <th>
                                        No
                                    </th>
                                    <th>
                                        Event Name
                                    </th>
                                    <th>
                                        Venue
                                    </th>
                                    <th>
                                        Date Start
                                    </th>
                                    <th>
                                        Date End
                                    </th>
                                    <th>
                                        Capacity
                                    </th>
                                    <th>
                                        &nbsp;
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                            @if($events == null)
                            <tr>
                                <td>
                                    No event planned yet!
                                </td>
                            </tr>
                            @endif
                            @php
                                $number = 1;
                            @endphp
                            @foreach($events as $event)
                                    <tr>
                                    <td>

                                        </td>
                                        <td>
                                        @php
                                                echo($number);
                                                $number++;
                                            @endphp
                                        </td>
                                        <td>
                                        {{ __($event['name'])}}
                                        </td>
                                        <td>
                                        {{ __($event['venue'])}}
                                        </td>
                                        <td>
                                        {{ __($event['from_date'])}} 
                                        </td>
                                        <td>
                                        {{ __($event['end_date'])}} 
                                        </td>
                                        <td>
                                        {{ __($event['capacity'])}} 
                                        </td>
                                        <td>
                                                <a class="btn btn-xs btn-primary" href="{{ url('/edit/'.$event['id']) }}">
                                                    {{ trans('global.edit') }}
                                                </a>

                                                <form  method="POST" action="{{ url('/deleteevent') }}" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                                @csrf
                                                    <input type="hidden" name="id" value="{{ $event['id'] }}">
                                                    <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                                </form>

                                        </td>

                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('user_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.users.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }

@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  $('.datatable-User:not(.ajaxTable)').DataTable({ buttons: dtButtons })
    $('a[data-toggle="tab"]').on('shown.bs.tab', function(e){
        $($.fn.dataTable.tables(true)).DataTable()
            .columns.adjust();
    });
})

</script>
@endsection