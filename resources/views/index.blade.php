<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Sweden: Social Democrats</title>

  <!-- Bootstrap core CSS -->
  <link href="{{ asset ('css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="{{ asset ('css/small-business.css') }}" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark fixed-top" style="background-color:#003399; color:yellow">
    <div class="container">
      <a class="navbar-brand" href="#">Sweden: Social Democrats</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item active">
            <a class="nav-link" href="#">Home
              <span class="sr-only">(current)</span>
            </a>
          </li>
          <!-- <li class="nav-item">
            <a class="nav-link" href="#">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Contact</a>
          </li> -->
          <li class="nav-item">
            @if (Auth::user()==null)
            <a class="nav-link js-scroll-trigger" href="{{ route('login') }}">Login</a>
            @endif
          </li>
          <li class="nav-item">
            @if (Auth::user()!=null)
            <a class="nav-link js-scroll-trigger" href="{{ route('logout') }}" onClick="return confirm('srs ah nak logout?')">Logout</a>
            @endif
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Content -->
  <div class="container">

    <!-- Heading Row -->
    <div class="row align-items-center my-5">
      <div class="col-lg-7" style="text-align:center">
        <img class="img-fluid rounded mb-4 mb-lg-0" src="img/logoparty.png" width="300"alt="Social Democrat logo">
      </div>
      <!-- /.col-lg-8 -->
      <div class="col-lg-5">
        <h1 class="font-weight-light" ><i>Socialdemokraterna</i></h1>
        <p>The Social Democratic Party – a labour party at its core – was founded in 1889, which makes it the oldest political party in Sweden.</p>
        <a class="btn btn-primary" href="{{ url('/register') }}">Join event as a member!</a>
      </div>
      <!-- /.col-md-4 -->
    </div>
    <!-- /.row -->

    <!-- Call to Action Well -->
    <div class="card text-white my-5 py-4 text-center" style="background-color:#003399; color:yellow">
      <div class="card-body" style="background-color:#003399; color:yellow">
        <p class="text-white m-0"><i>We believe in the equal value and right of all human beings</i></p>
      </div>
    </div>

    <!-- Content Row -->
    <div class="row">
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">History</h2>
            <p class="card-text">Founded in 1889, the SAP is the oldest and largest political party in Sweden. From the mid-1930s to the 1980s, the Social Democratic Party won more than 40% of the vote. Currently, the party has been heading the government since 2014 under Prime Minister Stefan Löfven, who has been the party leader since 2012.</p>
            <br>
          </div>
          <!--<div class="card-footer">
            <a href="#" class="btn btn-primary btn-sm">More Info</a>
          </div>-->
        </div>
      </div>
      <!-- /.col-md-4 -->
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">Organisational Structure</h2>
            <p class="card-text">Currently, the SAP has about 100,000 members, with about 2,540 local party associations and 500 workplace associations. It has been the largest party in the Riksdag since 1914. The member base is diverse, but it prominently features organized blue-collar workers and public sector employees.</p>
            <br>
          </div>
          <!-- <div class="card-footer">
            <a href="#" class="btn btn-primary btn-sm">More Info</a>
          </div> -->
        </div>
      </div>
      <!-- /.col-md-4 -->
      <div class="col-md-4 mb-5">
        <div class="card h-100">
          <div class="card-body">
            <h2 class="card-title">Our Values</h2>
            <ul class="card-text">
              <li>The creation of more jobs and active efforts against unemployment takes precedence over everything else.  </liu>
              <li>Welfare must be reliable, and it must be of high quality.</li>
              <li>Cohesion must increase, but this is not done through tax cuts, cuts and reduced wages - or for that matter by pointing out scapegoats and pitting groups against each other.</li>
              <li>We must reduce the gaps, break segregation and fight crime.</li>
              <li>We need a common community building, where everyone is involved and contributes and feels responsible for our country's development and future.</li>
            </ul><br>
            
            <!-- <p class="card-text"></p> -->
          </div>
          <!-- <div class="card-footer">
            <a href="#" class="btn btn-primary btn-sm">More Info</a>
          </div> -->
        </div>
      </div>
      <!-- /.col-md-4 -->

    </div>
    <!-- /.row -->

  </div>
  <!-- /.container -->

  <!-- Footer -->
  <footer class="py-5" style="background-color:#003399; color:yellow">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Sweden: Social Democrats</p>
    </div>
    <!-- /.container -->
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="{{ asset('jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

</body>

</html>
