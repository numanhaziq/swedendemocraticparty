<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/addEvent', function () {
    return view('create');
});

Route::get('/logout', function () {
    return view('logout');
});

//Route::redirect('/', '/login');
Route::redirect('/home', '/admin');
Auth::routes();



//Route for AddEventController
Route::post('/createnewevent', [App\Http\Controllers\Admin\AddEventController::class, 'store'])->name('createNewEvent'); //nk add new event
Route::get('/edit/{id}', [App\Http\Controllers\Admin\AddEventController::class, 'edit']); //Nk buka edit page for the chosen data
Route::post('/updateevent', [App\Http\Controllers\Admin\AddEventController::class, 'update'])->name('updateEvent'); //Update existing data
Route::post('/deleteevent', [App\Http\Controllers\Admin\AddEventController::class, 'destroy'])->name('deleteEvent'); //Delete chosen data

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');
});
