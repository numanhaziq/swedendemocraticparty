<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Events;

class EventController extends Controller
{
    protected function create(Request $request)
    {
        $event = $request;
        echo($event['from_date']);
        $dateFrom = $event['from_date'];
        $dateTo = $event['end_date'];

        $dateFrom = date('Y-m-d', strtotime(str_replace('-', '/', $dateFrom)));
        $dateTo = date('Y-m-d', strtotime(str_replace('-', '/', $dateTo)));

        Events::create([
            'name' => $event['eventName'],
            'venue' => $event['venue'],
            'description' => $event['description'],
            'capacity' => $event['capacity'],
            'from_date' => $dateFrom,
            'end_date' => $dateTo,
        ]);

        return redirect('/home');
        // $event->save();
        // return Events::create([
        //     'name' => $data['name'],
        //     'venue' => $data['venue'],
        //     'description' => $data['description'],
        //     'date' => $data['date'],
        //     'capacity' => $data['capacity'],
        // ]);
    }
}
