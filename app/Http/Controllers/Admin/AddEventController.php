<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\User;
use App\Event;
use Illuminate\Http\Request;
use DateTime;

class AddEventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = $request;
        
        $data_inverter1 = explode("/",$event['from_date']);
        $dateFrom = $data_inverter1[2].'-'. $data_inverter1[1].'-'. $data_inverter1[0];

        $data_inverter2 = explode("/",$event['end_date']);
        $dateTo = $data_inverter2[2].'-'. $data_inverter2[1].'-'. $data_inverter2[0];

        echo "|| date from : ".$dateFrom." || date to :".$dateTo;

        $dateFrom = date('Y-m-d', strtotime($dateFrom));
        $dateTo = date('Y-m-d', strtotime($dateTo));


        
        echo "<br>|| date from : ".$dateFrom." || date to :".$dateTo;


        Event::create([
            'name' => $event['eventName'],
            'venue' => $event['venue'],
            'description' => $event['description'],
            'capacity' => $event['capacity'],
            'from_date' => $dateFrom,
            'end_date' => $dateTo,
        ]);
        
        // $picture = $_FILES['fileToUpload']['name'];
        // $picture_tmp = $_FILES['fileToUpload']['tmp_name'];

        // $target2 = "images/".$event['eventName'].".jpg";
        // move_uploaded_file($picture_tmp,$target2);

        return redirect('/home');


        // $event->save();
        // return Events::create([
        //     'name' => $data['name'],
        //     'venue' => $data['venue'],
        //     'description' => $data['description'],
        //     'date' => $data['date'],
        //     'capacity' => $data['capacity'],
        // ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return view('edit', compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $dateFrom = date('Y-m-d', strtotime($request['from_date']));
        $dateTo = date('Y-m-d', strtotime($request['end_date']));


        $id = $request->input('id');
        $event = Event::find($id);

        if($dateFrom!=$event['from_date']){
            $data_inverter1 = explode("/",$request['from_date']);
            $dateFrom = $data_inverter1[2].'-'. $data_inverter1[1].'-'. $data_inverter1[0];
        }

        if($dateTo!=$event['end_date']){
            $data_inverter2 = explode("/",$request['end_date']);
            $dateTo = $data_inverter2[2].'-'. $data_inverter2[1].'-'. $data_inverter2[0];
        }
        echo " date from : ".$dateFrom."  date to :".$dateTo;

        $dateFrom = date('Y-m-d', strtotime($dateFrom));
        $dateTo = date('Y-m-d', strtotime($dateTo));

        $event['name'] = $request['eventName'];
        $event['venue'] = $request['venue'];
        $event['description'] = $request['description'];
        $event['capacity'] = $request['capacity'];
        $event['from_date'] = $dateFrom;
        $event['end_date'] = $dateTo;

        $event->save();

        return redirect('/home');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $id = $request->input('id');
        $event = Event::find($id);
        $event->delete();

        return redirect('/home');

    }
}
