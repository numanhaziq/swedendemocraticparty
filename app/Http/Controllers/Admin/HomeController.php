<?php

namespace App\Http\Controllers\Admin;

use App\Event;
use Illuminate\Support\Facades\Auth;

class HomeController
{
    public function index()
    {
        $user=Auth::user()->is_admin;
        $events = Event::all()->toArray();
        
        if($user==1){
            return view('home', compact('events'));
        }

        return view('index');
    }
}
