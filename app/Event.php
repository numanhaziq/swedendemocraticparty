<?php

namespace App;

//use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;


class Event extends Model
{
    use Notifiable;

    protected $fillable = [
        'name',
        'venue',
        'description',
        'capacity',
        'from_date',
        'end_date',
    ];
}
