<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name')->nullable();

            $table->date('from_date')->nullable();
            $table->date('end_date')->nullable();

            $table->string('description')->nullable();
            $table->integer('capacity');
            $table->timestamps();

            $table->softDeletes();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    
}
